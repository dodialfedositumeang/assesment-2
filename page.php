<?php 


get_header(); 

if ( is_front_page() ) { 

	get_template_part('template_parts/homepage'); 

} else { 
		
	if ( have_posts() ): 
		while ( have_posts() ): 
			the_post();

			the_title(); 
			the_content(); 

			get_sidebar();

		endwhile; 
	endif;
	
} 

get_footer(); 