<?php

	/*
		Template Name: Template Home
	*/

?>

<?php 

get_header(); 
	
if ( have_posts() ): 
	while ( have_posts() ): 
		the_post();

		the_title(); 
		the_content(); 

		get_sidebar();

	endwhile; 
endif;
	
get_footer(); 